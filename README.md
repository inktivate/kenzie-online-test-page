## Create headings on your first webpage

You are ready to start writing HTML! Let's start with heading tags. Use the editor below the video to follow along with me. Pause and rewind the video as needed.

[![Headings](https://i.vimeocdn.com/video/764737381_1280x720.jpg)](https://player.vimeo.com/video/321911629)

There's no way actual video embeds or CodePen editors can work inside a markdown file. Y'all crazy.